extends Node

var bgm_player
var BGM = "res://audio/forest.ogg"
#var click = "res://audio/FX/click.wav"

# Play Background Music
func play_bgm():
## Make Music Player for BGM
	bgm_player = AudioStreamPlayer.new()
	self.add_child(bgm_player)
	bgm_player.stream = load(BGM)
## Set bus to far right
	bgm_player.bus = "Volume"
	#bgm_player.volume_db = -20
	bgm_player.play()

# Play Sound
#func play_sound():
## Make Sound Player for Menu Clicks
	#var player = AudioStreamPlayer.new()
## When Sound Finished, Free the Player to reduce clutter
	#player.connect('finished', player, 'queue_free')
	#self.add_child(player)
	#player.stream = load(click)
	#player.volume_db = -15
	#player.play()