extends Node

# The Game's Dialogue

# Variables
## Nodes
#onready var player = get_tree().get_nodes_in_group("player")[0]
#onready var small_thing = get_tree().get_nodes_in_group("st")[0]#$"../../../../World/Room/SmallThing"
onready var interact_content = $".."
var current_line = 'start'

# character
# start
# next
# keys
# content
# Align - 2 is right (NPC) || 0 is left (Player)
onready var content_dict = {
	'st_dict': {
		'start': {'text': "20 more this morning--! There’s no end to them.", 'align': 2, 'next': 'a2'},
		'a2': {'text': "We’re at the end of our budget.", 'align': 0, 'next': 'a3'},
		'a3': {'text': "The dogs can’t stay on the street. At this rate--", 'align': 2, 'next': 'a4'},
		'a4': {'text': "I stressed the inevitability of euthanization and I will again.", 'align': 0, 'next': 'a6'},
		'a6': {'text': "Adoption rates are down and we cannot continue to run at a loss.", 'align': 0, 'next': 'a7'},
		'a7': {'text': "These numbers aren’t normal, though! We need to get at the heart.", 'align': 2, 'next': 'a8'},
		'a8': {'text': "If your colleagues fell through, get on the streets and find the real problem.", 'align': 2, 'next': 'a9'},
		'a9': {'text': "--Me? I’m an accountant, not a dog-catcher.", 'align': 0, 'next': 'b1'},
		'b1': {'text': "And as our accountant, you’ll investigate our risk factors. Go.", 'align': 2, 'next': 'zz'},
		'zz': {'text': "", 'align': 0, 'next': 'end'}
		}
}

func _on_Next_pressed():
	var interact_name = $"../../Name"
	var next_button = $"../../Next"
	current_line = content_dict['st_dict'][current_line]['next']
	if content_dict['st_dict'][current_line]['next'] == 'end':
		$"../../../..".end_dialogue()
	else:
		interact_content.new_text(content_dict['st_dict'][current_line])
		if content_dict['st_dict'][current_line].has('function'):
			content_dict['st_dict'][current_line]['obj'].call(content_dict['st_dict'][current_line]['func'], content_dict['st_dict'][current_line]['para'])