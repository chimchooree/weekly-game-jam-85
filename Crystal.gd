extends Node

func _ready():
	add_to_group("thing")

func _on_KinematicBody2D_body_entered(body):
	#print($"KinematicBody2D".name)
	for body in $"KinematicBody2D".get_overlapping_bodies():
		if body.get_node("..").is_in_group("player"):
			$Sprite.hide()
			$"Sprite/Sprite".hide()
			$"KinematicBody2D".hide()
			body.get_node("..").buy_item("crystal")