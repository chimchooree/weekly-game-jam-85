extends Node

# The Player Character
# In group "character"

# Variable
var res
var display_name = "Small Thing"
onready var portrait = $"../../../Interation/CanvasLayer/Interactionbox/smallthing"
var space
onready var aDog = $"KinematicBody2D/aDog"
onready var bounds = $"../Bounds/CollisionShape2D"#get_tree().get_nodes_in_group("room")[0].get_node($Bounds)
onready var animated_sprite = $KinematicBody2D/AnimatedSprite
var key = "st"
var first_time = true
var current_line = 'start'
var timer_make
var win_timer
var won = false

func find_space():
	randomize()
	var pos_x = randi() % int(bounds.shape.extents[1])#int(get_viewport().size.x)
	var pos_y = randi() % int(bounds.shape.extents[0])
	var space = Vector2(bounds.global_position.x + pos_x, bounds.global_position.y + pos_y)
	return space

func buy_item(item):
	print ("buy")

func make_new(new_res):
	timer_make.stop()
	res = new_res
	print ("new " + new_res)
	timer_make()
	if new_res == "res://IceCream.tscn":
		$KinematicBody2D/aDog.texture = preload("res://art/ui/anic.png")
		timer_win(new_res)
	if new_res == "res://Bomb.tscn":
		$KinematicBody2D/aDog.texture = preload("res://art/ui/thatthing.png")
		timer_win(new_res)

func timer_win(new_res):
	$"../../../Interation/CanvasLayer/Interactionbox/System".text = "Small Thing is distracted from dogs..."
	won = true
	win_timer = Timer.new()
	win_timer.wait_time = 7
	win_timer.connect('timeout', self, 'win', [new_res])
	add_child(win_timer)
	win_timer.start()

func win(new_res):
	print ("yay")
	win_timer.stop()
	if new_res == "res://IceCream.tscn":
		get_tree().change_scene("res://Ending_IceCream.tscn")
	if new_res == "res://Bomb.tscn":
		get_tree().change_scene("res://Ending_Bomb.tscn")

func make_more(res):
	print ("more " + res)
	aDog.show()
	timer_emote(aDog)
	var thing = load(res)
	thing = thing.instance()
	thing.get_node("KinematicBody2D").global_position = find_space()
	add_child(thing)

func emote_off(emote, timer):
	timer.queue_free()
	emote.hide()

func timer_emote(emote):
	var timer = Timer.new()
	timer.wait_time = .25
	timer.connect('timeout', self, 'emote_off', [emote, timer])
	add_child(timer)
	timer.start()

func timer_make():
	timer_make = Timer.new()
	randomize()
	timer_make.wait_time = randi() % 10
	timer_make.connect('timeout', self, 'make_more', [res])
	add_child(timer_make)
	timer_make.start()
	
func _physics_process(delta):
		animated_sprite.play()
		animated_sprite.animation = "idle"

func _ready():
	add_to_group("character")
	add_to_group("st")
	$KinematicBody2D.position = $Position2D.position
	res = "res://Dog.tscn"
	timer_make()