extends Area2D

var signal_connected = false
onready var rigid_body = $".."
var offset
var impulse

func _on_body_entered(body):
	#print ("BODY ENTERED DOG")
	#rigid_body.add_force(body.position, get_tree().get_nodes_in_group("UserControl")[0].velocity)
	#rigid_body.add_force(body.position - rigid_body.position, get_tree().get_nodes_in_group("UserControl")[0].velocity)
	offset = body.position
	impulse = -1 * (body.position - rigid_body.position)
	rigid_body.add_force(offset, impulse)
	rigid_body.applied_torque = 0

func _on_body_exited(body):
	#print ("BODY EXITED DOG")
	rigid_body.add_force(-1 * offset, -1 * impulse)
	rigid_body.applied_torque = 0

func _ready():
	if not signal_connected:
		#print ("Dog ready")
		connect('body_entered', self, '_on_body_entered')
		connect('body_exited', self, '_on_body_exited')
		signal_connected = true