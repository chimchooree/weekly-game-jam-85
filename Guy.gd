extends Node

# The Player Character
# In group "player"

enum STATES {ALIVE, DEAD}
var state = ALIVE
var can_move = true
var display_name = "Hamish"
onready var portrait = $"../../../Interation/CanvasLayer/Interactionbox/MarketingCoordinator"
var key = "0"
var inv = ["blackderry", "money", "dog"]
var current_line = 'start'
var partner
var system

func system_off(timer):
	timer.queue_free()
	system.hide()

func timer_system():
	var timer = Timer.new()
	timer.wait_time = 10
	timer.connect('timeout', self, 'system_off', [timer])
	add_child(timer)
	timer.start()

func buy_item(item):
	system = get_tree().get_nodes_in_group("interact")[0].get_node("CanvasLayer/Interactionbox/System")
	system.text = "Obtained " + item
	system.show()
	inv.append(item)
	timer_system()
	if item == "ice cream":
		get_tree().get_nodes_in_group("merch")[0].get_node("KinematicBody2D/Quest").hide()

func _ready():
	add_to_group("player")
	$KinematicBody2D.position = $Position2D.position