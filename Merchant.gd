extends Node

var display_name = "Julian, Local Trader"
onready var portrait = $"../../../Interation/CanvasLayer/Interactionbox/MarketingCoordinator"
onready var animated_sprite = $KinematicBody2D/AnimatedSprite
var key = "ju"
var first_time = true
var current_line = 'start'
# use to "Purchased Melon Soda!"

func emote_off(emote, timer):
	timer.queue_free()
	emote.hide()

func timer_emote(emote):
	var timer = Timer.new()
	timer.wait_time = .25
	timer.connect('timeout', self, 'emote_off', [emote, timer])
	add_child(timer)
	timer.start()

func timer_make():
	var timer_make = Timer.new()
	randomize()
	timer_make.wait_time = randi() % 10
	add_child(timer_make)
	timer_make.start()
	
func _physics_process(delta):
		animated_sprite.play()
		animated_sprite.animation = "idle"

func _ready():
	add_to_group("character")
	add_to_group("merch")
	$KinematicBody2D.position = $Position2D.position