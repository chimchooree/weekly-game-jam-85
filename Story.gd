extends Node

# The Game's Dialogue

# Variables
## Nodes
var player# = get_tree().get_nodes_in_group("player")[0]
onready var small_thing = get_tree().get_nodes_in_group("st")[0]#$"../../../../World/Room/SmallThing"
onready var merchant = get_tree().get_nodes_in_group("merch")[0]#$"../../../../World/Room/SmallThing"
onready var interact_name = $"../../Name"
onready var interact_content = $".."
onready var next_button = $"../../Next"
var current_line
var partner

# Align - 2 is right || 0 is left
onready var content_dict = {
	'st_dict': {
		# Small Thing
		'st' : {
	# Loop
			'start': {'text': "What are you doing up here, miss?", 'align': 0, 'next': 'a2'},
			'a2': {'text': "Whoa- what's this guy?", 'align': 2, 'next': 'a3'},
			'a3': {'text': "Pardon?", 'align': 0, 'next': 'a4'},
			'a4': {'text': "A dog!", 'align': 2, 'next': 'a6', 'function': 'true', 'obj': small_thing, 'func': "make_more", 'para': "res://Dog.tscn"},
			'a6': {'text': "–! Did you...how did you?", 'align': 0, 'next': 'a7'},
			'a7': {'text': "I'm a Small Thing That Makes Things.", 'align': 2, 'next': 'a8'},
			'a8': {'text': "I can make anything I want! I like dogs! A dog!", 'align': 2, 'next': 'a9', 'function': 'true', 'obj': small_thing, 'func': "make_more", 'para': "res://Dog.tscn"},
			'a9': {'text': "Well, surely there are other things she’d like besides these stupid dogs.", 'align': 0, 'next': 'b1'},
			'b1': {'text': "Maybe around town?", 'align': 0, 'next': 'zz'},
			# If have items, show buttons for those items. 
			# She likes it or not.
			# Unlock an ending.
			'zz': {'text': "", 'align': 0, 'next': 'end'},
			
			'second': {'text': "You say there's better things than dogs? Not really.", 'align': 2, 'next': 'z2'},
			'z2': {'text': "", 'align': 0, 'next': 'end'},
			
			'third': {'text': "Are you sure there's better things than dogs?", 'align': 2, 'next': 't2'},
			't2': {'text': "Absolutely. Let me check my pockets...", 'align': 0, 'next': 't3'},
			't3': {'text': "", 'align': 1, 'next': 'z3', 'function': 'true', 'obj': interact_content, 'func': "show_buttons", 'para': ""},
			'z3': {'text': "", 'align': 1, 'next': 'end'},
			
			'blackderry': {'text': "I couldn't live without my BlackDerry.", 'align': 0, 'next': 'bd2'},
			'bd2': {'text': "Those are for old people. Wouldn't you rather have dog?", 'align': 2, 'next': 'zbd'}, #, 'function': 'true', 'obj': small_thing, 'func': "make_more", 'para': "res://Dog.tscn"},
			'zbd': {'text': "", 'align': 1, 'next': 'end'},
			
			'bomb': {'text': "Whoa. What is that thing!", 'align': 2, 'next': 'bo2'},
			'bo2': {'text': "Sorry, I meant to grab something else.", 'align': 0, 'next': 'bo3'},
			'bo3': {'text': "No, I like that thing!", 'align': 2, 'next': 'zbd', 'function': 'true', 'obj': small_thing, 'func': "make_new", 'para': "res://Bomb.tscn"},

			'money': {'text': "If you make things, why don't you make lots of money?", 'align': 0, 'next': 'mo2'},
			'mo2': {'text': "Why? Money doesn't do anything.", 'align': 2, 'next': 'mo3'},
			'mo3': {'text': "Huh. Money is just a middle man for you.", 'align': 0, 'next': 'zbd'},
			
			'ice cream': {'text': "Ice cream is assuredly better than most things.", 'align': 0, 'next': 'ic2'},
			'ic2': {'text': "It looks testy. What is that?", 'align': 2, 'next': 'ic3'},
			'ic3': {'text': "Go ahead and taste!", 'align': 0, 'next': 'ic4'},
			'ic4': {'text': "In fact, you could be Julian's new supplier if you make more.", 'align': 0, 'next': 'ic5'},
			'ic5': {'text': "Amazing...I like ice cream. An ice cream!", 'align': 2, 'next': 'zbd', 'function': 'true', 'obj': small_thing, 'func': "make_new", 'para': "res://IceCream.tscn"},

			'dog': {'text': "...wow. Dogs are way better than dogs.", 'align': 2, 'next': 'do2'},
			'do2': {'text': "Businessmen are actually pretty smart.", 'align': 2, 'next': 'zbd'},
			
			'crystal': {'text': "See this here? Shimmery, isn't it?", 'align': 0, 'next': 'cr2'},
			'cr2': {'text': "It's pretty thing. What is it?", 'align': 2, 'next': 'cr3'},
			'cr3': {'text': "It's a crystal. They're rare and beautiful.", 'align': 0, 'next': 'cr4'},
			'cr4': {'text': "Oh.", 'align': 2, 'next': 'cr5'},
			'cr5': {'text': "You like it, right?", 'align': 0, 'next': 'cr6'},
			'cr6': {'text': "Yeah.", 'align': 2, 'next': 'cr7'},
			'cr7': {'text': "Okay, I'll find something more exciting.", 'align': 0, 'next': 'zbd'},
			},
		# Julian, the Merchant
		'ju' : {
			'start': {'text': "Hey Julian. One ice cream, please.", 'align': 0, 'next': 'a1'},
			'a1': {'text': "Sure, man. You're getting the last one.", 'align': 2, 'next': 'a2'},
			'a2': {'text': "Savor it. My supplier has been missing for two weeks.", 'align': 2, 'next': 'a3', 'function': 'true', 'obj': get_tree().get_nodes_in_group("player")[0], 'func': "buy_item", 'para': "ice cream"},
			'a3': {'text': "I won't get more in until I go to the warehouse myself...", 'align': 2, 'next': 'zz'},
			'zz': {'text': "", 'align': 0, 'next': 'end'}
			}
		}
}

func _on_Next_pressed():
	player = get_tree().get_nodes_in_group("player")[0]
	for body in player.get_node("KinematicBody2D/RangeBubble").get_overlapping_bodies():
		partner = body.get_node("..")
		if partner.is_in_group("character"):
			current_line = content_dict['st_dict'][$"..".partner_key][partner.current_line]['next']
			partner.current_line = current_line
			if content_dict['st_dict'][$"..".partner_key][current_line]['next'] == 'end':
				get_tree().get_nodes_in_group("UserControl")[0].end_dialogue(partner)
			else:
					# Set Text
				interact_content.set_text(content_dict['st_dict'][partner.key][current_line])
				if content_dict['st_dict'][partner.key][current_line].has('function'):
					content_dict['st_dict'][$"..".partner_key][current_line]['obj'].call(content_dict['st_dict'][partner.key][current_line]['func'], content_dict['st_dict'][partner.key][current_line]['para'])

func _on_Blackberry_pressed():
	interact_content.hide_buttons()
	current_line = content_dict['st_dict']['st']['blackderry']['next']
	small_thing.current_line = current_line
	interact_content.set_text(content_dict['st_dict']['st']['blackderry'])
	current_line = 'blackderry'
	small_thing.current_line = current_line

func _on_Bomb_pressed():
	interact_content.hide_buttons()
	current_line = content_dict['st_dict']['st']['bomb']['next']
	small_thing.current_line = current_line
	interact_content.set_text(content_dict['st_dict']['st']['bomb'])
	current_line = 'bomb'
	small_thing.current_line = current_line

func _on_Money_pressed():
	interact_content.hide_buttons()
	current_line = content_dict['st_dict']['st']['money']['next']
	small_thing.current_line = current_line
	interact_content.set_text(content_dict['st_dict']['st']['money'])
	current_line = 'money'
	small_thing.current_line = current_line

func _on_IceCream_pressed():
	interact_content.hide_buttons()
	current_line = content_dict['st_dict']['st']['ice cream']['next']
	small_thing.current_line = current_line
	interact_content.set_text(content_dict['st_dict']['st']['ice cream'])
	current_line = 'ice cream'
	small_thing.current_line = current_line

func _on_Dog_pressed():
	interact_content.hide_buttons()
	current_line = content_dict['st_dict']['st']['dog']['next']
	small_thing.current_line = current_line
	interact_content.set_text(content_dict['st_dict']['st']['dog'])
	current_line = 'dog'
	small_thing.current_line = current_line

func _on_Crystal_pressed():
	interact_content.hide_buttons()
	current_line = content_dict['st_dict']['st']['crystal']['next']
	small_thing.current_line = current_line
	interact_content.set_text(content_dict['st_dict']['st']['crystal'])
	current_line = 'crystal'
	small_thing.current_line = current_line