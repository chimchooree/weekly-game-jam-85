extends Node

# UserControlAI is the mind of the Player Character. It limits the Player's actions through what it wills to do.
# It also manages Input
# Group: "UserControl"

# Variables
## Nodes
onready var user = $".." 
onready var small_thing = get_tree().get_nodes_in_group("st")[0]
onready var sprite = $"../KinematicBody2D" # PC's Sprite
onready var range_bubble = $"../KinematicBody2D/RangeBubble" # Range of PC
onready var animated_sprite = $"../KinematicBody2D/AnimatedSprite"
var interact
var player_name
var interact_name
var interact_content
var next_button
var item_button
var story
var partner # interact partner portrait
var signal_connected = false # skill_pressed()
var interact_target = null
## Player Condition
enum STATES {ALIVE, DEAD} # Whether the PC is alive
## Movement
var velocity = Vector2()
export (int) var speed = 150
var set_fixed_process = false

export (bool) var is_player = true

#signal box_opened
const GRAVITY = 1000.0 # pixels/second/second

# Angle in degrees towards either side that the player can consider "floor"
const WALK_FORCE = 600
const WALK_MIN_SPEED = 10
const WALK_MAX_SPEED = 200
const STOP_FORCE = 1300
const JUMP_SPEED = 500
const JUMP_MAX_AIRBORNE_TIME = 0.2
const CLIMB_SPEED = 2

var on_air_time = 100
var jumping = false
#var on_ladder = false

var prev_jump_pressed = false


func interact_input():
	if Input.is_action_just_pressed('interact') and not small_thing.won:
		print ("hey")
		interact = get_tree().get_nodes_in_group("interact")[0]
		player_name = interact.get_node("CanvasLayer/Interactionbox/PlayerName")
		interact_name = interact.get_node("CanvasLayer/Interactionbox/Name")
		interact_content = interact.get_node("CanvasLayer/Interactionbox/Content")
		story = interact.get_node("CanvasLayer/Interactionbox/Content/Story")
		next_button = interact.get_node("CanvasLayer/Interactionbox/Next")
		item_button = get_tree().get_nodes_in_group("item_button")
		for body in range_bubble.get_overlapping_bodies():
			partner = body.get_node("..")
			if partner.is_in_group("character"):
				print (partner.first_time)
			# Player
				if partner.first_time:
					user.can_move = false
					user.portrait.show()
					player_name.show()
					player_name.text = user.display_name
			# Partner
					if partner != user:
						interact_name.show()
						interact_content.partner_key = partner.key
						interact_content.partner = partner
						interact_name.text = partner.display_name
						partner.portrait.show()
			# Content
					interact_content.show_text()
					interact_content.show()
			# Button
					next_button.show()
				else:
					if partner == small_thing:
						user.can_move = false
						user.portrait.show()
						player_name.show()
						player_name.text = user.display_name
			# Partner
						interact_name.show()
						interact_content.partner_key = partner.key
						interact_content.partner = partner
						interact_name.text = partner.display_name
						partner.portrait.show()
			# Content
						if user.inv.has("ice cream") or user.inv.has("bomb") or user.inv.has("crystal"):
							interact_content.show_third_text()
							partner.current_line = 'third'
						else:
							interact_content.show_second_text()
							partner.current_line = 'second'
						interact_content.show()
			# Button
						next_button.show()

func end_dialogue(c):
	for body in range_bubble.get_overlapping_bodies():
		user.can_move = true
	# Player
		user.portrait.hide()
		player_name.hide()
	# Partner
		interact_name.hide()
		c.portrait.hide()
		interact.get_node("CanvasLayer/Interactionbox/smallthing").hide()
	# Content
		interact_content.hide()
		c.first_time = false
	# Button
		next_button.hide()
		for b in item_button.size():
			item_button[b].hide()

func _physics_process(delta):
	# Keyboard Movement
	get_input()
	if user.state == ALIVE && user.can_move:

		check_collision_with_stone_rounded()

	# Create forces
		var force = Vector2(0, GRAVITY)
	
		var walk_left = Input.is_action_pressed("left")
		var walk_right = Input.is_action_pressed("right")
		var jump = Input.is_action_pressed("jump")

		var active = walk_left or walk_right or jump

		var stop = true

		if walk_left:
			if velocity.x <= WALK_MIN_SPEED and velocity.x > -WALK_MAX_SPEED:
				force.x -= WALK_FORCE
				stop = false
		elif walk_right:
			if velocity.x >= -WALK_MIN_SPEED and velocity.x < WALK_MAX_SPEED:
				force.x += WALK_FORCE
				stop = false
	
		if stop:
			var vsign = sign(velocity.x)
			var vlen = abs(velocity.x)
			vlen -= STOP_FORCE * delta
			if vlen < 0:
				vlen = 0
			velocity.x = vlen * vsign
	
		# Integrate forces to velocity
		velocity += force * delta	
	# Integrate velocity into motion and move
		velocity = sprite.move_and_slide(velocity, Vector2(0, -1))
	
		if sprite.is_on_floor():
			on_air_time = 0

		if jumping and velocity.y > 0:
		# If falling, no longer jumping
			jumping = false
	
		if on_air_time < JUMP_MAX_AIRBORNE_TIME and jump and not prev_jump_pressed and not jumping:
		# Jump must also be allowed to happen if the character left the floor a little bit ago.
		# Makes controls more snappy.
			velocity.y = -JUMP_SPEED
			jumping = true
	
		if velocity.length() > 0:
			animated_sprite.play()
		else:
			animated_sprite.play()
			animated_sprite.animation = "idle"
		if velocity.x != 0:
			animated_sprite.animation = "run"
			animated_sprite.flip_v = false
			animated_sprite.flip_h = velocity.x < 0
		elif (jumping):	
			pass#animated_sprite.animation = "jump"

		on_air_time += delta
		prev_jump_pressed = jump

func get_tile_on_position(x,y):
	var tilemap = get_parent().get_parent().get_parent().get_node("Room/Room/Ground")
	if not tilemap == null:
		var map_pos = tilemap.world_to_map(Vector2(x,y))
		var id = tilemap.get_cellv(map_pos)
		if id > -1:
			var tilename = tilemap.get_tileset().tile_get_name(id)
			return tilename
		else:
			return ""

func check_collision_with_stone_rounded():
	var x = sprite.position.x
	var y = sprite.position.y - 50
	if get_tile_on_position(x,y) == "box":
		var tilemap = get_parent().get_node("TileMap")
		if not tilemap == null:
			var map_pos = tilemap.world_to_map(Vector2(x,y))
			tilemap.set_cellv(map_pos, -1)

##########

# Get Player Input
func get_input():
## Interact
	interact_input()
## Keyboard Movement
#	get_movement_input()

func _on_died(deader):
# Receive from character/died()
	if deader == user:
		user.state = deader.state

# Ready
func _ready():
	add_to_group("UserControl")