extends Node

# Little things make more.
# Little things make more of things they like.
# Don't let Little things find things that are dangerous or bad because they'll probably like them. 
# Small things are bad things, so the attraction is natural.

# She's at top, like DK, and dogs rain down. Get to the top and find her.
# Find something else she likes.
# Bombs are the bad ending.

# You start out with a helmet.

# Level 1 - room with you, Little thing, and an item you need more of (health potions).
# "Wha--there's more? How did you do that?" (I'm a small thing that makes things!) "A what?" (Hehe!)
# Bring the item to the little thing to make more of it.
# You win when she makes more enough times.

# Level 2 - room with you, little thing, a bad thing, and an exit. 
# The little thing will gravitate towards the bad thing and make more of it.
# Try to get to the exit without getting hurt.

# Level 3 - room with you, little thing, and armor.
# "I need more armor if she's going to make dangerous things like that..."
# Take the armor to the little thing
# She refuses. "I'm not really interested in that...What even is it?"
# I guess I'm going to have to negotiate.
# Convince the little thing of the appeal of the armor.
# "It'll keep me safe!" (You've been fine so far..)
# "I can fight in it!" (Whoa, you think so!! I want to fight people..)
# "It has a beautiful design!" (Not really..)
# "It's worth a lot of money!" (Who needs money?)
# Then she'll make more. You will automatically equip the armor when you walk over it. She'll equip the first set.

# Level 4 - 
# "Health potions, That's not what I truly desire, though.."


# "You should know everything you need to know about little things."
# Little things make more. 

# Ideas
# Negotiate with the little thing to find the true appeal of some things they refuse to like.
# Script Kitty
# Cat Man - office game. Then Online Cat Man.

func _ready():
	Audio.play_bgm()