# Small Thing That Makes Things

Play in Browser @ https://chimchooree.itch.io/small-thing-that-makes-things

Play as the accountant of an overcrowded shelter on the edge of bankruptcy. 
Government grants and private donors have failed you. Now, you must get your 
feet on the ground and uncover the truth of the recent explosion of the local 
stray population.

Made for Weekly Game Jam 85. It's a shallow adventure platformer without much 
challenge, but it does have two endings. A lot of content was cut because I had 
to finish fast. I like it, though, because it's my first finished game made for 
me, not for class. Learned a lot!

## Controls

* Up - W, Up Arrow, Up stick
* Left - A, Left Arrow, Left Stick
* Down - S, Down Arrow, Down Stick
* Right - D, Right Arrow, Right Stick
* Interact - Space, XBOX A, PS X
* Jump - E, XBOX Y, PS triangle

## Characters

* Hamish T. Elwell - The hero accountant. Alma mater, University of Chicago.
* Candice Steele - Head dog catcher. Took the position to make a difference.
* Small Thing - Easily excited but also easily bored.
* Julian Waits - The ice cream man. His first big reinvestment will go to a stall.

## Notes

* Controller controls aren't tested well

## Credits

* coding + art by chimchooree
* Open Pixel Project (animations & tiles, edited to fit my characters & palette) @ http://www.openpixelproject.com/
* Music track "forest" by syncopika under CC-BY 3.0 @ https://opengameart.org/content/forest & https://greenbearmusic.bandcamp.com/album/bgm-fun-vol-5
* Bad ending image from Wikimedia, credit to Jon Sullivan @ https://tinyurl.com/y6oswx8v (url contains spoilers)
* SimpleJPC-16 Palette by Adigun Polack @ https://lospec.com/palette-list/simplejpc-16
* pixel joy font by chimchooree @ https://fontstruct.com/fontstructions/show/1596262/pixel-joy
* Tools: Godot Engine 3.0.6, GraphicsGale
* Weekly Game Jam 85, "Offspring" theme @ https://itch.io/jam/weekly-game-jam-85

#WeeklyGameJam #WGJGame 