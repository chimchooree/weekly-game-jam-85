extends Label

onready var story = $Story
var partner_key
var partner
var player

func show_text():
#	if first_time:
	text = story.content_dict['st_dict'][partner_key]['start']['text']
	align = story.content_dict['st_dict'][partner_key]['start']['align']

func show_second_text():
	text = story.content_dict['st_dict']['st']['second']['text']
	align = story.content_dict['st_dict']['st']['second']['align']

func show_third_text():
	text = story.content_dict['st_dict']['st']['third']['text']
	align = story.content_dict['st_dict']['st']['third']['align']

func set_text(new_text):
	text = new_text['text']
	align = new_text['align']

func show_buttons(nothing):
	$"../Next".hide()
	player = get_tree().get_nodes_in_group("player")[0]
	if player.inv.has("blackderry"):
		$"../Blackberry".show()
	if player.inv.has("money"):
		$"../Money".show()
	if player.inv.has("dog"):
		$"../Dog".show()
	if player.inv.has("ice cream"):
		$"../IceCream".show()
	if player.inv.has("bomb"):
		$"../Bomb".show()
	if player.inv.has("crystal"):
		$"../Crystal".show()

func hide_buttons():
	for b in get_tree().get_nodes_in_group("item_button"):
		b.hide()
	$"../Next".show()
	text = ""
	show()

func _ready():
	hide()