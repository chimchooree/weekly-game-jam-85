extends Node

var splash = 0

func _on_TextureButton_pressed():
	if splash == 0:
		$Credits.show()
		$Title.hide()
	if splash >= 1:
		$Credits.hide()
		$BG.show()
		$Splash.hide()
		$Interation/Interactionbox/Name.show()
		$Interation/Interactionbox/PlayerName.show()
		$Interation/Interactionbox/Content.show()
		$Interation/Interactionbox/MarketingCoordinator.show()
		$Interation/Interactionbox/Next.show()
		$Interation/Interactionbox/Candice.show()
		$Interation/Interactionbox/Content.set_text($Interation/Interactionbox/Content/Story.content_dict['st_dict']['start']['text'])
	splash = splash + 1

func end_dialogue():
	get_tree().change_scene("res://Main.tscn")