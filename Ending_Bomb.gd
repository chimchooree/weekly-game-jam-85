extends Node

var count = get_children()

var splash = 0

func _on_TextureButton_pressed():
	for child in count:
		child.hide()
	if splash == 0:
		$Sprite.show()
	if splash == 1:
		$Sprite2.show()
	if splash == 2:
		$Sprite3.show()
	if splash == 3:
		$Sprite4.show()
	if splash == 4:
		$Sprite5.show()
	if splash >= 5:
		get_tree().quit()
	splash = splash + 1
	$TextureButton.show()